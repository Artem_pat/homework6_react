import { createAction, createAsyncThunk } from "@reduxjs/toolkit"
import { sendRequest } from "../helpers/sendRequest"

export const actionFetchData = createAsyncThunk(
    "fetchData",
    async () => {
        const saveProducts = JSON.parse(localStorage.getItem("products"));
        if (saveProducts) {
            return saveProducts
        } else {
            const data = await sendRequest("/index.json");
            return data.map((product) => {
                return {...product, isFavorite: false, isBasket: false, basketCounter: 0}
            })
        }
    }
)

export const actionAddFavorite = createAction("ACTION_ADD_FAVORITE")
export const actionRemoveFavorite = createAction("ACTION_REMOVE_FAVORITE")
export const actionAddBasket = createAction("ACTION_ADD_BASKET")
export const actionRemoveBasket = createAction("ACTION_REMOVE_BASKET")
export const actionIncreaseBasket = createAction("ACTION_INCREASE_BASKET")
export const actionDecreaseBasket = createAction("ACTION_DECREASE_BASKET")
export const actionSendBuyInfo = createAction("ACTION_SEND_BUY_INFO")