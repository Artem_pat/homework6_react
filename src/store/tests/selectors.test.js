import {selectorFavorite, selectorBasket, selectorUserForm} from "../selectors"

describe("redux selectors", () => {
    test("selectorFavorite selector", () => {
        const favorite = [{
            name: "Футболка чоловіча",
            price: 250,
            image: "1.jpg",
            article: "1234567890",
            color: "чорний"
          }]
        const result = selectorFavorite({favorite});
        expect(result).toEqual(favorite)
    })
    test("selectorBasket selector", () => {
        const basket = [{
            name: "Футболка чоловіча",
            price: 250,
            image: "1.jpg",
            article: "1234567890",
            color: "чорний"
          }]
        const result = selectorBasket({basket});
        expect(result).toEqual(basket)
    })
    test("selectorUserForm selector", () => {
        const userForm = {
            name: "John",
            lastname: "Alex",
            age: "20",
            address: "Ukraine",
            phone: "+38066999888777",
        }
        const result = selectorUserForm({userForm});
        expect(result).toEqual(userForm)
    })
})