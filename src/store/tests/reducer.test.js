import rootReducer from "../reducer"
import * as actions from "../actions"

describe("Testing reducers", () => {
    const initialState = {
        favorite: [],
        basket: [],
        products: [],
        userForm: {
            name: "",
            lastname: "",
            age: "",
            address: "",
            phone: "",
        },
    };
    test("testing actionAddBasket", () => {
        const item = { article: 123, name: 'Test Item', price: 100 }
        const action = actions.actionAddBasket(item)
        const result = rootReducer(initialState, action)
        expect(result.basket).toEqual([{...item, isBasket: true, basketCounter: 1}])
    })
    test("testing actionAddBasket with identical item", () => {
        const item = { article: 123, name: 'Test Item', price: 100, isBasket: true, basketCounter: 1 }
        const sameItem = {article: 123, name: 'Test Item', price: 100}
        const action = actions.actionAddBasket(sameItem)
        initialState.basket = [item];
        const result = rootReducer(initialState, action)
        expect(result.basket).toEqual([{...item, basketCounter: 2}])
    })
    test("testing actionRemoveBasket", () => {
        const item1 = { article: 111, name: 'Test Item', price: 100, isBasket: true, basketCounter: 1 }
        const item2 = { article: 222, name: 'Test Item 2', price: 200, isBasket: true, basketCounter: 1 }
        const action = actions.actionRemoveBasket(item1)
        initialState.basket = [item1, item2];
        console.log(initialState);
        const result = rootReducer(initialState, action)
        expect(result.basket).toEqual([item2])
    })
})