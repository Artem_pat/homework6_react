import FavoriteItem from "./FavoriteItem";
import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../../store/index"

const mockData = {
    image: 'item.jpg',
    name: 'Test Item',
    price: 100,
    article: '12345',
    color: 'red'
};

const handleClick = jest.fn()

describe("Testing FavoriteItem component", () => {
    test("Testing component FavoriteItem with data props", () => {
        render(<Provider store={store}><FavoriteItem data={mockData} /></Provider>)
        expect(screen.getByText('Test Item')).toBeInTheDocument()
        expect(screen.getByText('article: 12345')).toBeInTheDocument()
        expect(screen.getByText('color: red')).toBeInTheDocument()
        expect(screen.getByText('100 грн')).toBeInTheDocument()
    })
    test("testing onClick", () => {
        render(<Provider store={store}><FavoriteItem data={mockData} onDel={handleClick}/></Provider>)
        const button = screen.getByRole("button", { name: /Add To Cart/i })
        fireEvent.click(button)
        expect(handleClick).toHaveBeenCalled()
    })
    test("snapshot", () => {
        const favoriteItem = render(<Provider store={store}><FavoriteItem data={mockData} onDel={handleClick}/></Provider>)
        expect(favoriteItem).toMatchSnapshot()
    })
    
})