import Button from "./Button.jsx"
import React from "react"
import { render, screen, fireEvent } from "@testing-library/react"

const handleClick = jest.fn()
describe("Testing Button component", () => {
    test("children props", () => {
        render(<Button>test</Button>)
        expect(screen.getByText("test")).toBeInTheDocument()
    })
    test("onClick props", () => {
        const { container } = render(<Button onClick={handleClick}></Button>)
        const button = container.firstChild
        fireEvent.click(button)
        expect(handleClick).toHaveBeenCalled()
    })
    test("classname props", () => {
        render(<Button className={"_text"}>text</Button>)
        expect(screen.getByText("text")).toHaveClass("_text")
    })
    test("classname props", () => {
        render(<Button className={"_text"}>text</Button>)
        expect(screen.getByText("text")).toHaveClass("_text")
    })
    test("default classname props", () => {
        render(<Button>text</Button>)
        expect(screen.getByText("text")).toHaveClass("btn")
    })
    test("type props", () => {
        render(<Button>text</Button>)
        expect(screen.getByText("text")).toHaveAttribute("type","button")
    })
})