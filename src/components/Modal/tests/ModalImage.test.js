import ModalImage from "../ModalImage";
import React from "react";

import { render, screen, fireEvent, getByText } from "@testing-library/react"


const handleClickYes = jest.fn()
const handleClickNo = jest.fn()
describe("Testing ModalImage component", () => {
    test("Testing prop name", () => {
        render(<ModalImage name="test" />)
        expect(screen.getByText(/test/i)).toBeInTheDocument()
    })
    test("Testing prop color", () => {
        render(<ModalImage color="red" />)
        expect(screen.getByText(/red/i)).toBeInTheDocument()
    })
    test("Testing prop price", () => {
        render(<ModalImage price={100} />)
        expect(screen.getByText(/100/i)).toBeInTheDocument()
    })
    test("Testing prop addBasket and close on yes button", () => {
        render(<ModalImage addBasket={handleClickYes} close={handleClickNo}/>)
        const yesButton = screen.getByRole("button", {name: /yes/i})
        fireEvent.click(yesButton)
        expect(handleClickYes).toHaveBeenCalled()
        expect(handleClickNo).toHaveBeenCalled()
    })
    test("Testing prop close on no button", () => {
        render(<ModalImage close={handleClickNo}/>)
        const noButton = screen.getByRole("button", {name: /no/i})
        fireEvent.click(noButton)
        expect(handleClickNo).toHaveBeenCalled()
    })
    test("Testing image prop", () => {
        const { container } = render(<ModalImage name={"test"} image={"img.jpg"} />)
        const imageElement = container.querySelector(".img")
        expect(imageElement).toHaveAttribute("alt", "test")
        expect(imageElement).toHaveAttribute("src", "images/img.jpg")
    })
})