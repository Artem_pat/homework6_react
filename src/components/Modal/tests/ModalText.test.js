import React from "react"
import ModalText from "../ModalText"
import { render, screen, fireEvent } from "@testing-library/react"

const handleclose = jest.fn()
const handleAddFavorite = jest.fn()
describe("Testing ModalText component", () => {
    test("Testing prop name", () => {
        render(<ModalText name="test" />)
        expect(screen.getByText(/test/i)).toBeInTheDocument()
    })
    test("Testing prop color", () => {
        render(<ModalText color="red" />)
        expect(screen.getByText(/red/i)).toBeInTheDocument()
    })
    test("Testing prop price", () => {
        render(<ModalText price={100} />)
        expect(screen.getByText(/100/i)).toBeInTheDocument()
    })
    test("Testing onClick", () => {
        render(<ModalText close={handleclose} addFavorite={handleAddFavorite} />)
        const btn = screen.getByRole("button", {name: /add to favorite/i})
        fireEvent.click(btn)
        expect(handleclose).toHaveBeenCalled()
        expect(handleAddFavorite).toHaveBeenCalled()
    })
})