import React from "react";
import ModalWrapper from "./ModalWrapper.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalClose from "./ModalClouse.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalFooter from "./ModalFooter.jsx";
import "./Modal.scss"
import ModalContent from "./ModalContent.jsx";

const ModalImage = ({ close, name, color, price, image, addBasket }) => {

    return (
        <ModalWrapper onClick={close}>
            <ModalBody className="modal">
                <ModalClose onClick={close} />
                <img className="img" src={`images/${image}`} alt={name} />
                <ModalHeader>
                    Додати до корзини: {name}
                </ModalHeader>
                <ModalContent>
                    <p>колір: {color}</p>
                    <p>ціна: {price}грн</p>
                </ModalContent>
                <ModalFooter firstClick={close} firstText="no" secondaryClick={() => {
                    close()
                    addBasket()
                }} secondaryText="yes" />
            </ModalBody>
        </ModalWrapper>
    )

}

export default ModalImage;