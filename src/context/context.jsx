import { createContext, useState } from "react"

export const ToggleContext = createContext({})

const ToggleProvider = ({ children }) => {
    const [displayTog, setDisplayTog] = useState("table")
    const toggleDisplay = (name) => {
       setDisplayTog((prev) => prev = name)
    }
    return (
        <ToggleContext.Provider
            value={
                {
                    displayTog,
                    toggleDisplay
                }
            }
        >
            {children}
        </ToggleContext.Provider>
    )
}

export default ToggleProvider;