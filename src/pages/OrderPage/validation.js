import * as yup from "yup";

export const validation = yup.object({
    name: yup
        .string()
        .required('name is required')
        .min(3, "name is too short! min 3")
        .max(21, "name is too long! max 21")
        .matches(/[a-zA-Z]/, "only letters allowed"),
    lastname: yup
        .string()
        .required('lastname is required')
        .min(3, "name is too short! min 3")
        .max(21, "name is too long! max 21")
        .matches(/[a-zA-Z]/, "only letters allowed"),
    age: yup
        .number()
        .typeError('Age must be a number')
        .required('Age is required')
        .positive('Age must be positive number')
        .min(0, 'Age must be at least 0')
        .max(120, 'Age must be at most 120')
        .integer('Age must be integer number'),
    address: yup
        .string()
        .required('Adress is required'),
    phone: yup.string()
        .matches(/^[\d+]+$/, "Only digits are allowed")
        .min(13, "You dont enter a phone")
        .required('Phone number is required'),
})