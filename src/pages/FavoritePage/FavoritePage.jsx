import React, { useState } from "react";
import FavoriteItem from "../../components/Items/FavoriteItem";
import "./FavoritePage.scss"
import { useSelector, useDispatch } from "react-redux";
import { actionRemoveFavorite, actionAddBasket } from "../../store/actions"
import ModalImage from "../../components/Modal/ModalImage"
import { selectorFavorite } from "../../store/selectors"

const FavoritePage = () => {
    const [isModal, setIsModal] = useState(false)
    const [currentDate, setCurrentDate] = useState({})
    // const localFavorite = JSON.parse(localStorage.getItem("favorite"))
    const favorite = useSelector(selectorFavorite)
    const dispatch = useDispatch()

    const delFavoriteItem = (data) => {
        dispatch(actionRemoveFavorite(data))
    }

    const handleAddBasket = (item) => {
        dispatch(actionAddBasket(item))
    }

    const handleCurrentData = (item) => {
        setCurrentDate(item)
    }

    const handleModalImage = () => {
        setIsModal(!isModal)
    }

    return (
        <>
            <h2 className="favorite-title">Wish List</h2>
            {favorite.map((item, index) => {
                return <FavoriteItem key={index} data={item} onDel={() => {
                    handleModalImage()
                    handleCurrentData(item)
                }} />
            })}
            {isModal && <ModalImage
            close={handleModalImage}
            addBasket={() => {
                handleAddBasket(currentDate)
                delFavoriteItem(currentDate)
            }}
            name={currentDate.name}
            color={currentDate.color}
            price={currentDate.price}
            image={currentDate.image}
            />}

        </>
    )
}

export default FavoritePage;