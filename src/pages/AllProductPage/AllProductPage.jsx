import React, { useState, useEffect, useContext } from "react";
import "./AllProductPage.scss"
import Product from "../../components/Product.jsx";
import ModalText from "../../components/Modal/ModalText.jsx";
import ModalImage from "../../components/Modal/ModalImage.jsx";
import { useDispatch, useSelector } from "react-redux"
import { actionFetchData, actionAddFavorite, actionAddBasket } from "../../store/actions.js"
import { selectorAllProducts } from "../../store/selectors.js"
import TableIcon from "../../assets/icons/cards_24dp_FILL0_wght400_GRAD0_opsz24.svg?react"
import RowIcon from "../../assets/icons/drag_indicator_24dp_FILL0_wght400_GRAD0_opsz24.svg?react"
import cn from "classnames"
import {ToggleContext} from "../../context/context.jsx";

const AllProductPage = () => {
    // const [isModalText, setIsModalText] = useState(false);
    const [isModalImage, setIsModalImage] = useState(false);
    const [currentDate, setCurrentDate] = useState({});
    const {displayTog, toggleDisplay} = useContext(ToggleContext);
    console.log(displayTog);
    const cards = useSelector(selectorAllProducts);

    const dispatch = useDispatch();

    const handleCurrentData = (item) => {
        setCurrentDate(item)
    }

    // const handleModalText = () => setIsModalText(!isModalText)
    const handleModalImage = () => setIsModalImage(!isModalImage)

    const handleAddFavorite = (data) => {
        dispatch(actionAddFavorite(data))
    }

    const handleAddBasket = (data) => {
        dispatch(actionAddBasket(data))
    }

    useEffect(() => {
        dispatch(actionFetchData())
    }, []);

    return (
        <>
            <div className="toggle-wrapper">
                <TableIcon name="table" onClick={(e) => toggleDisplay(e.currentTarget.getAttribute('name'))} className={cn("icon-toggle", {"active" : displayTog === "table"})} />
                <RowIcon name="row" onClick={(e) => toggleDisplay(e.currentTarget.getAttribute('name'))} className={cn("icon-toggle", {"active" : displayTog === "row"})} />
            </div>
            <div className={cn({"products-box-table" : displayTog === "table", "products-box-row" : displayTog === "row"})}>
                {cards?.map((item, index) => {
                    return <Product onClick={() => {
                        handleCurrentData(item)
                    }} key={index} 
                    data={item} 
                    onFavotite={() => {
                        handleAddFavorite(item)
                    }} 
                    onImg={handleModalImage} />
                })}
            </div>
            {isModalImage && <ModalImage close={handleModalImage}
                addBasket={() =>
                    handleAddBasket(currentDate)
                }
                name={currentDate.name}
                color={currentDate.color}
                price={currentDate.price}
                image={currentDate.image}
            />}
            {/* {isModalText && <ModalText close={handleModalText}
                addFavorite={() => {
                    handleAddFavorite(currentDate)
                }}
                name={currentDate.name}
                color={currentDate.color}
                price={currentDate.price}
            />} */}
        </>
    )
}

export default AllProductPage;